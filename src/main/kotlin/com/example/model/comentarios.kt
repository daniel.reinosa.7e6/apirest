package com.example.model

import kotlinx.serialization.Serializable
import java.util.*
@Serializable
data class Comentario(
    val id: Int,
    val peliculaId: Int,
    val comentario: String,
    val fechaCreacion: String)

val almacenComentarios = mutableListOf<Comentario>()
