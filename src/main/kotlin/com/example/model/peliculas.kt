package com.example.model

import kotlinx.serialization.Serializable
import java.util.*
@Serializable
data class Pelicula(
    val id: Int,
    var titulo: String,
    var anio: Int,
    var genero: String,
    var director: String,
    val comentario: MutableList<Comentario>
)

val almacenPeliculas = mutableListOf<Pelicula>()