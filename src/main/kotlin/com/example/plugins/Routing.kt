package com.example.plugins

import com.example.model.Comentario
import com.example.model.Pelicula
import com.example.model.almacenComentarios
import com.example.model.almacenPeliculas
import io.ktor.http.*
import io.ktor.server.routing.*
import io.ktor.server.response.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import java.util.*

fun Application.configureRouting() {
    fun Route.movieRouting(){
        route("/movies"){
            get {
                if (almacenPeliculas.isNotEmpty()) call.respond(almacenPeliculas)
                else call.respondText("No movies found.", status = HttpStatusCode.OK)
            }
            get("{id?}") {
                if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                    "Missing id",
                    status = HttpStatusCode.BadRequest
                )
                val id = call.parameters["id"]
                for (movie in almacenPeliculas) {
                    if (movie.id.toString() == id) return@get call.respond(movie)
                }
                call.respondText( "No movie with id $id", status = HttpStatusCode.NotFound)
            }
            post {
                val movie = call.receive<Pelicula>()
                almacenPeliculas.add(movie)
                call.respondText("Movie stored correctly", status = HttpStatusCode.Created)
            }
            post {
                val movie = call.receive<Pelicula>()
                val review = call.receive<Comentario>()
                movie.comentario.add(review)
                call.respondText("Review stored correctly", status = HttpStatusCode.Created)
            }
            delete("{id?}") {
                if (call.parameters["id"].isNullOrBlank()) return@delete call.respondText(
                    "Missing id",
                    status = HttpStatusCode.BadRequest
                )
                val id = call.parameters["id"]
                for (movie in almacenPeliculas) {
                    if (almacenPeliculas.removeIf { it.id.toString() == id }) {
                        call.respondText("Movie removed correctly", status = HttpStatusCode.Accepted)
                    } else {
                        call.respondText("Movie not Found", status = HttpStatusCode.NotFound)
                    }
                }
            }
            put("{id?}") {
                if (call.parameters["id"].isNullOrBlank()) return@put call.respondText(
                    "Missing id",
                    status = HttpStatusCode.BadRequest
                )
                val id = call.parameters["id"]
                val movieUpdated = call.receive<Pelicula>()
                for (movie in almacenPeliculas) {
                    if (movie.id.toString() == id) {
                        movie.titulo = movieUpdated.titulo
                        movie.anio = movieUpdated.anio
                        movie.genero = movieUpdated.genero
                        movie.director = movieUpdated.director
                        return@put call.respondText(
                            "La pelicula $id ha sido actualizada",
                            status = HttpStatusCode.Accepted
                        )
                    }
                }
                call.respondText(
                    "Movie with id $id not found",
                    status = HttpStatusCode.NotFound
                )
            }
            route("{id?}/reviews") {
                post {
                    val addNewReview = call.receive<Comentario>()
                    if (call.parameters["id"].isNullOrBlank()) {
                        return@post call.respondText("Missing ID!", status = HttpStatusCode.BadRequest)
                    }
                    val id = call.parameters["id"]
                    for (i in almacenPeliculas.indices) {
                        if (almacenPeliculas[i].id.toString() == id) {
                            almacenPeliculas[i].comentario.add(addNewReview)
                        }
                    }
                    call.respondText("Review stored correctly", status = HttpStatusCode.Created)
                }
                get {
                    if (call.parameters["id"].isNullOrBlank()) {
                        return@get call.respondText("Missing ID", status = HttpStatusCode.BadRequest)
                    }
                    val id = call.parameters["id"]
                    for (i in almacenPeliculas.indices) {
                        if (almacenPeliculas[i].id.toString() == id) {
                            for (j in almacenPeliculas[i].comentario) {
                                return@get call.respond(j)
                            }
                            call.respondText("Review not found", status = HttpStatusCode.NotFound)
                        }
                    }
                    call.respondText("Movie with id $id not found", status = HttpStatusCode.NotFound)
                }
            }}}}



